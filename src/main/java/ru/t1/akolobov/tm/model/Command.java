package ru.t1.akolobov.tm.model;

public final class Command {

    public Command() {
    }

    public Command(String name, String argument, String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    private String name = "";

    private String argument;

    private String description = "";

    public void setName(String name) {
        this.name = name;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getArgument() {
        return argument;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (description != null && !description.isEmpty()) result += " : " + description;
        return result;
    }

}
