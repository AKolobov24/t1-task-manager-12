package ru.t1.akolobov.tm.api.controller;

import ru.t1.akolobov.tm.model.Task;

public interface ITaskController {
   
    void changeTaskStatusById();
    
    void changeTaskStatusByIndex();
    
    void clearTasks();

    void completeTaskById();

    void completeTaskByIndex();

    void createTask();

    void displayTasks();

    void displayTask(Task task);

    void displayTaskById();

    void displayTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

    void removeTaskById();

    void removeTaskByIndex();

    void startTaskById();

    void startTaskByIndex();
}
