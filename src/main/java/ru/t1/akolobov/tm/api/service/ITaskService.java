package ru.t1.akolobov.tm.api.service;

import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.model.Task;

import java.util.List;

public interface ITaskService {

    Task add(Task task);

    Task changeStatusById(String id, Status status);

    Task changeStatusByIndex(Integer index, Status status);

    Task create(String name);

    Task create(String name, String description);

    void clear();

    List<Task> findAll();

    void remove(Task task);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task removeById(String id);

    Task removeByIndex(Integer index);

}
